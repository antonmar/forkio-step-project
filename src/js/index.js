const dropDownMenu = document.querySelector(".header__dropdown-menu-lines");
const body = document.body;
const lines = document.querySelectorAll(".header__dropdown-menu-line");
const hiddenPartOfMenu = document.querySelector(
  ".header__dropdown-menu-hidden-part"
);

body.addEventListener("click", (ev) => {
  console.log(ev.target);
  if((!ev.target.classList.contains("header__dropdown-menu-hidden-part") &&
      (!ev.target.classList.contains("header__dropdown-menu-lines")) &&
      (!ev.target.classList.contains("header__dropdown-menu-line")) &&
      (!ev.target.classList.contains("header__dropdown-menu-element"))&&
      (!ev.target.classList.contains("header__dropdown-menu-element-line"))&&
      (!ev.target.classList.contains("header__dropdown-menu-text"))
  )){
    hiddenPartOfMenu.classList.add("hidden");

  }if((!ev.target.classList.contains("header__dropdown-menu-hidden-part") &&
        (!ev.target.classList.contains("header__dropdown-menu-lines")) &&
        (!ev.target.classList.contains("header__dropdown-menu-line")) &&
        (!ev.target.classList.contains("header__dropdown-menu-element"))&&
        (!ev.target.classList.contains("header__dropdown-menu-element-line"))&&
        (!ev.target.classList.contains("header__dropdown-menu-text"))
    )){
        dropDownMenu.style.background = "none";

        lines.forEach((e) => {
            e.classList.remove("hidden");
        })
    }

})

dropDownMenu.addEventListener("click", (evt) => {
  hiddenPartOfMenu.classList.toggle("hidden");
    lines.forEach((e) => {
        e.classList.toggle("hidden");
        if (e.classList.contains("hidden")){
            dropDownMenu.style.background = "url('/dist/img/cross.png') center no-repeat";
        }
        else {
            dropDownMenu.style.background = "none";
        }

    })

});
